package live.match.service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

final class Scoreboard {
    private final Map<String, Match> matchMap;
    private final Comparator<Match> matchComparator;


    Scoreboard(Map<String, Match> matchMap, Comparator<Match> matchComparator) {
        this.matchMap = matchMap;
        this.matchComparator = matchComparator;
    }

    String getSummary() {
        int index = 0;
        StringBuilder summeryBuilder = new StringBuilder();
        boolean newLine = false;
        for (Match match : getMatchList()) {
            if (newLine) {
                summeryBuilder.append(System.lineSeparator());
            }
            newLine = true;
            summeryBuilder.append(generateSummeryRow(++index, match));
        }
        return summeryBuilder.toString();
    }

    List<Match> getMatchList() {
        return matchMap.values().stream()
                .sorted(matchComparator)
                .toList();
    }

    private static String generateSummeryRow(int index, Match match) {
        return index + ". " +
                match.getHomeTeam().name() + " " + match.getHomeTeamScore() +
                " - " +
                match.getAwayTeam().name() + " " + match.getAwayTeamScore();
    }

}