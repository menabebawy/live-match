package live.match.service;

import java.util.HashMap;

public interface MatchService {
    Match start(String homeTeamName, String awayTeamName) throws StartNewMatchException;

    Match update(String id,
                 int homeTeamScore,
                 int awayTeamScore) throws MatchNotFoundException, InvalidMatchStateException;

    void finish(String id) throws MatchNotFoundException;

    String getSummary();

    static MatchService createInstance() {
        return new MatchServiceImpl(new HashMap<>());
    }
}