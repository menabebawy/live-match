package live.match.api;

import live.match.service.InvalidMatchStateException;
import live.match.service.Match;
import live.match.service.MatchNotFoundException;
import live.match.service.MatchService;
import live.match.service.StartNewMatchException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LiveScoreboardApiImplTest {
    LiveScoreboardApi liveScoreboardApi;
    MatchService matchService;

    @BeforeEach
    void setUp() {
        matchService = mock(MatchService.class);
        liveScoreboardApi = new LiveScoreboardApiImpl(matchService);
    }

    @Test
    void testStartNewMatch() throws StartNewMatchException {
        Match mockMatch = mock(Match.class);
        when(matchService.start("homeTeamName", "awayTeamName")).thenReturn(mockMatch);
        Match match = liveScoreboardApi.startNewMatch("homeTeamName", "awayTeamName");
        assertThat(match).isEqualTo(mockMatch);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n", "  \t  ", "\t \n  "})
    void testThrowIllegalArgumentExceptionWhenStartMatchByInvalidHomeTeamName(String blankTeamName) {
        assertThatThrownBy(() -> liveScoreboardApi.startNewMatch(blankTeamName, "awayTeamName"))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n", "  \t  ", "\t \n  "})
    void testThrowIllegalArgumentExceptionWhenStartMatchByInvalidAwayTeamName(String blankTeamName) {
        assertThatThrownBy(() -> liveScoreboardApi.startNewMatch("homeTeamName", blankTeamName))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testThrowIllegalArgumentExceptionWhenUpdateMatchByInvalidHomeTeamScore() {
        assertThatThrownBy(() -> liveScoreboardApi.updateMatch("id", -1, 1))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testThrowIllegalArgumentExceptionWhenUpdateMatchByInvalidAwayTeamScore() {
        assertThatThrownBy(() -> liveScoreboardApi.updateMatch("id", 1, -1))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n", "  \t  ", "\t \n  "})
    void testThrowIllegalArgumentExceptionWhenUpdateMatchByInvalidId(String id) {
        assertThatThrownBy(() -> liveScoreboardApi.updateMatch(id, 1, 1))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testUpdateMatch() throws MatchNotFoundException, InvalidMatchStateException {
        Match mockUpdatedMatch = mock(Match.class);
        when(matchService.update("id", 1, 0)).thenReturn(mockUpdatedMatch);
        Match updatedMatch = liveScoreboardApi.updateMatch("id", 1, 0);
        assertThat(updatedMatch).isEqualTo(mockUpdatedMatch);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n", "  \t  ", "\t \n  "})
    void testThrowIllegalArgumentExceptionWhenFinishMatchByInvalidId(String id) {
        assertThatThrownBy(() -> liveScoreboardApi.finishMatch(id))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testFinishMatch() throws MatchNotFoundException {
        doNothing().when(matchService).finish("id");
        liveScoreboardApi.finishMatch("id");
        verify(matchService, times(1)).finish("id");
    }

    @Test
    void testGetSummaryScoreboard() {
        String mockSummary = "";
        when(matchService.getSummary()).thenReturn(mockSummary);
        String summary = liveScoreboardApi.getScoreboardSummary();
        assertThat(summary).isEqualTo(mockSummary);
    }

}