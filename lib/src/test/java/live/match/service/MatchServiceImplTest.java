package live.match.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class MatchServiceImplTest {
    MatchService matchService;

    Map<String, Match> matchRepository;

    @BeforeEach
    void setup() {
        matchRepository = new HashMap<>();
        matchService = new MatchServiceImpl(matchRepository);
    }

    @Test
    void testStartNewMatch() throws StartNewMatchException {
        Match match = matchService.start("HomeTeam", "AwayTeam");
        assertThat(match.getScore()).isZero();
        assertThat(match.getHomeTeam().name()).isEqualTo("HomeTeam");
        assertThat(match.getAwayTeam().name()).isEqualTo("AwayTeam");
    }

    @Test
    void testThrowStartNewMatchExceptionWhenHomeTeamIsCurrentlyPlaying() {
        Match homeMatch = new Match(UUID.randomUUID().toString(),
                                    System.nanoTime(),
                                    new Team("Home"),
                                    new Team("Paris"));
        matchRepository.put(homeMatch.getId(), homeMatch);

        assertThatThrownBy(() -> matchService.start("Home", "Away"))
                .isInstanceOf(StartNewMatchException.class);
    }

    @Test
    void testThrowStartNewMatchExceptionWhenAwayTeamIsCurrentlyPlaying() {
        Match awayMatch = new Match(UUID.randomUUID().toString(),
                                    System.nanoTime(),
                                    new Team("Canada"),
                                    new Team("Away"));
        matchRepository.put(awayMatch.getId(), awayMatch);

        assertThatThrownBy(() -> matchService.start("Austria", "Away"))
                .isInstanceOf(StartNewMatchException.class);
    }

    @Test
    void testThrowInvalidMatchStateExceptionWhenUpdateMatchByHomeScoreLessThanCurrent() {
        Match currentMatch = new Match("id", System.nanoTime(), new Team("homeTeam"), new Team("awayTeam"));
        currentMatch.setTeamsScores(2, 1);
        matchRepository.put(currentMatch.getId(), currentMatch);

        assertThatThrownBy(() -> matchService.update("id", 1, 2))
                .isInstanceOf(InvalidMatchStateException.class);
    }

    @Test
    void testThrowInvalidMatchStateExceptionWhenUpdateMatchByAwayScoreLessThanCurrent() {
        Match currentMatch = new Match("id", System.nanoTime(), new Team("homeTeam"), new Team("awayTeam"));
        currentMatch.setTeamsScores(2, 1);
        matchRepository.put(currentMatch.getId(), currentMatch);

        assertThatThrownBy(() -> matchService.update("id", 2, 0))
                .isInstanceOf(InvalidMatchStateException.class);
    }

    @Test
    void testUpdateMatch() throws InvalidMatchStateException, MatchNotFoundException {
        Match currentMatch = new Match("id", System.nanoTime(), new Team("homeTeam"), new Team("awayTeam"));
        currentMatch.setTeamsScores(2, 1);
        matchRepository.put(currentMatch.getId(), currentMatch);

        Match updatedMatch = matchService.update("id", 2, 2);

        assertThat(updatedMatch.getScore()).isEqualTo(4);
        assertThat(updatedMatch.getAwayTeamScore()).isEqualTo(2);
        assertThat(currentMatch.getHomeTeamScore()).isEqualTo(2);
    }

    @Test
    void testThrowMatchNotFoundExceptionWhenUpdateMatch() {
        assertThatThrownBy(() -> matchService.update("id", 2, 1))
                .isInstanceOf(MatchNotFoundException.class);
    }

    @Test
    void testThrowMatchNotFoundExceptionWhenFinishMatch() {
        assertThatThrownBy(() -> matchService.finish("id")).isInstanceOf(MatchNotFoundException.class);
    }

    @Test
    void testFinishMatch() {
        Match currentMatch = new Match("id", System.nanoTime(), new Team("homeTeam"), new Team("awayTeam"));
        matchRepository.put(currentMatch.getId(), currentMatch);
        assertDoesNotThrow(() -> matchService.finish("id"));
    }

    @Test
    void testGetScoreboardForEmptyScoreboard() {
        assertThat(matchService.getSummary()).isEmpty();
    }

}